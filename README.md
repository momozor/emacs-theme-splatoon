# emacs-theme-splatoon
Emacs Theme: Splatoon

![ss](https://user-images.githubusercontent.com/24475030/54983336-90fc3c00-4fe7-11e9-9a85-de90ba871f70.png)


## Installation
1. Launch emacs

2. Hit `Alt+x` and type `package-install-file` and hit `Enter`.

3. Type the path where `lyre-theme.el` located and hit `Enter`.

4. Again, hit `Alt+x` and type `load-theme` and hit `Enter`.

5. Then type `Splatoon` to load the theme.

## Authors
* Momozor

## License
This project is licensed under the GPL-3.0 license. See LICENSE file for more details.
